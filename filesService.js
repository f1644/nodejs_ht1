const fs = require('fs');
const path = require('path');

const filesPath = './files/'

function createFile (req, res, next) {
  try {
    if (!req.body.content) {
      res.status(400).send({ "message": "Please specify 'content' parameter" });
    } else if (!req.body.filename) {
      res.status(400).send({ "message": "Please specify 'filename' parameter" });
    } else {
      fs.writeFileSync(
        `${filesPath}${req.body.filename}`,
        req.body.content,
        'utf8'
      );
      res.status(200).send({ "message": "File created successfully" });
    }
  } catch (e) {
    res.status(500).send({ "message": "Server error" });
  }
}

function getFiles (req, res, next) {
  try {
    fs.readdir(filesPath, (err, files) => {
      res.status(200).send({
        "message": "Success",
        "files": files
      })
    }); 
  } catch(e) {
    res.status(400).send({ "message": "Client error" });
  }
}

const getFile = async (req, res, next) => {
  try {
    const fileName = req.params.filename;
    const filePath = `${filesPath}${fileName}`;
    const content = await fs.promises.readFile(filePath, 'utf8');
    const stat = await fs.promises.stat(filePath, 'utf8');
    const extension = path.extname(fileName).substring(1);

    res.status(200).send({
      "message": "Success",
      "filename": fileName,
      "content": content,
      "extension": extension,
      "uploadedDate": stat.birthtime});
  } catch(e) {
    res.status(400).send({ "message": `No file with ${req.params.filename} filename found` });
  }
  
}

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile
}
